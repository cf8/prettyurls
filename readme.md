# Pretty Urls System #

## Why we need it: ##
Offers and hubs have a long url. If you want to enter URL on the mobile device - be reserved by patience.

## How it works: ##
Open in your browser this page: http://t/ (“t” page)
Copy some long URL and paste in the “Url” field at the “t” page. Click “Add url!”. New url will be added to the top of urls list. Remember number, left of the link.
Take your mobile device and open the page “t/go/{number}”. Example: t/go/5 .

## Running a python dev server: ##
### Prerequisites: ###
* python 2.6
* pip
* virtualenv
* mongodb
### the flow: ###

```
#!bash

$ cd /path/to/cloned/project/
$ virtualenv --no-site-packages env
$ source env/bin/activate
[env]$ pip install -r deps.txt
[env]$ ./main.py
```
## Deploy using uwsgi: ##
### Prerequisites: ###
* Apache2 web server compiled with mod_proxy
* uwsgi (http://projects.unbit.it/uwsgi/)
* mongodb

### The flow: ###
* add/uncomment this lines in https's config:

```
#!

LoadModule              proxy_module            modules/mod_proxy.so
LoadModule              proxy_http_module       modules/mod_proxy_http.so
```
* add new virtual host into httpd's config:
```
#!

<VirtualHost *:80>
ServerName t
ProxyPass / http://localhost:3031/
</VirtualHost>
```

* modify uwsgi.ini file to your needs (change paths)
* run uwsgi instanse like so:

```
#!bash

$ /usr/bin/uwsgi --pidfile /var/run/uwsgi_urlz.pid -d --ini /var/www/urlz/uwsgi.ini
```
or look for your linux distribution prefrered run method (init.d/rc.d scripts and so on)
